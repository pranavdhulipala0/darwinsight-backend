const mongoose = require("mongoose");
const reverseGeocode = require("../helpers/reverseGeocoding");
const Application = require("../models/ApplicationSchema");
const Services = require("../models/ServicesSchema");


exports.temp = (req, res) => {
  res.json({ message: "In auth controller" });
};

exports.test = async (req, res) => {
  console.log("I got called into test");
  res.send("Welcome to API");
  
};

exports.searchMetric= async(req,res)=>{
  const {tenantId, applicationId, query}= req.body;
  const metricType = "SEARCHMETRIC";

  try {
    let application = await Application.findOne({applicationId});

    if(!application){
      return res.status(404).json({error: "Application not found"})
    }

    const today = new Date();
    const dateKey = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    const dateString = dateKey.toISOString().split("T")[0];

    let metric = application.services[metricType].find(
        (item) => item.date.toISOString().split("T")[0] === dateString
    );

    if (!metric) {
        // If today's date does not exist, create a new metric document
        const newMetric = {
            date: dateKey,
            searchQueries: []
        };
        application.services[metricType].push(newMetric);
        metric = newMetric;

        // Save the updated application document immediately to ensure the new date document is created
        await application.save();
    }

      metric.searchQueries.push(query);

      await application.save();
    
    res.status(200).json({ message: "Metric updated successfully" });
  } catch (error) {
    console.error("Error occurred:", error);
      return res.status(500).json({ error: "Internal Server Error" });
  }
}

exports.geoMetric = async (req, res) => {
  const { tenantId, applicationId ,latitude,longitude} = req.body;
  const metricType = "GEOMETRIC";
  try {
      let application = await Application.findOne({ applicationId });

      if (!application) {
          console.log("Application not found");
          return res.status(404).json({ error: "Application not found" });
      }

      const location = await reverseGeocode(latitude,longitude);

      const today = new Date();
      const dateKey = new Date(today.getFullYear(), today.getMonth(), today.getDate());
      const dateString = dateKey.toISOString().split("T")[0];

      let metric = application.services[metricType].find(
          (item) => item.date.toISOString().split("T")[0] === dateString
      );

      if (!metric) {
          // If today's date does not exist, create a new metric document
          const newMetric = {
              date: dateKey,
              metrics: [{location:location,counts:1}]
          };
          application.services[metricType].push(newMetric);
          metric = newMetric;

          // Save the updated application document immediately to ensure the new date document is created
          await application.save();
      }

      // Check if the location exists in metrics array
      let locationMetric = metric.metrics.find((item) => item.location === location);

      if (!locationMetric) {
          // If the location does not exist, create a new document with counts as 1
          locationMetric = {
              location: location,
              counts: 1
          };
          metric.metrics.push(locationMetric);
      } else {
          // If the location exists, increment the count
          locationMetric.counts++;
      }

      // Save the updated application document
      await application.save();

      res.status(200).json({ message: "Metric updated successfully" });
  } catch (error) {
      console.error("Error occurred:", error);
      return res.status(500).json({ error: "Internal Server Error" });
  }
}



exports.countMetric = async (req, res) => {
  const { tenantId, applicationId } = req.body;
  const metricType = "COUNTMETRIC"; 

  try {
    let application = await Application.findOne({ applicationId });

    if (!application) {
      console.log("Application not found");
      return res.status(404).json({ error: "Application not found" });
    }

    const today = new Date();
    const dateKey = new Date(
      today.getFullYear(),
      today.getMonth(),
      today.getDate()
    );
    const dateString = dateKey.toISOString().split("T")[0];

    let metric = application.services[metricType].find(
      (item) => item.date.toISOString().split("T")[0] === dateString
    );

    if (!metric) {
      metric = {
        date: dateKey,
        counts: 1,
      };
      application.services[metricType].push(metric);
    }

    metric.counts++;

    await application.save();

    console.log("Count incremented successfully");
    return res.status(200).json({ message: "Count incremented successfully" });
  } catch (error) {
    console.error("Error occurred:", error);
    return res.status(500).json({ error: "Internal Server Error" });
  }
};

exports.performanceMetric = async (req, res) => {
  const { tenantId, applicationId, performance } = req.body;
  const metricType = "PERFORMANCEMETRIC"; 

  try {
    let application = await Application.findOne({ applicationId });

    if (!application) {
      console.log("Application not found");
      return res.status(404).json({ error: "Application not found" });
    }

    const today = new Date();
    const dateKey = new Date(
      today.getFullYear(),
      today.getMonth(),
      today.getDate()
    );
    const dateString = dateKey.toISOString().split("T")[0];

    let metric = application.services[metricType].find(
      (item) => item.date.toISOString().split("T")[0] === dateString
    );

    if (!metric) {
      metric = {
        date: dateKey,
        count: 0, 
        totalPerformance: 0 
      };
      application.services[metricType].push(metric);
    }

    metric.count++;
    metric.totalPerformance += performance;

    metric.avgPerformance = metric.totalPerformance / metric.count;

    await application.save();

    console.log("Performance recorded successfully");
    return res.status(200).json({ message: "Performance recorded successfully" });
  } catch (error) {
    console.error("Error occurred:", error);
    return res.status(500).json({ error: "Internal Server Error" });
  }
};



exports.fetchMetrics = async (req, res) => {
  const { tenantId, applicationId, metricType, startDate, endDate } = req.body;
  try {
    const servicesPrefix = "$services." + metricType;
    const serviceDatePrefix = "services." + metricType + ".date";
    const serviceCountsPrefix = "$services." + metricType + ".counts";
    const matchQuery = {};

    let pipeline = [
      {
        $match: {
          applicationId: applicationId
        }
      },
      {
        $unwind: servicesPrefix
      },
      {
        $match: {
          [serviceDatePrefix]: {
            $gte: new Date(startDate ? startDate : "1970-01-01"),
            $lte: new Date(endDate ? endDate : "9200-01-01"),
          },
        },
      },
      {
        $project: {
          [metricType]: servicesPrefix
        }
      },
    ];

    // Execute the aggregation pipeline
    const documents = await Application.aggregate(pipeline);
    console.log(documents);
    res.json(documents);
    return documents;
  } catch (error) {
    console.error("Error occurred:", error);
    throw error; // Propagate error to the caller
  }
};

exports.insertDummyData = async (req, res) => {
  const sampleData = [
    { "date": "2024-01-01", "avgPerformance": Math.floor(Math.random() * 100) + 1, "totalPerformance": Math.floor(Math.random() * 100) + 1 },
    { "date": "2024-01-05", "avgPerformance": Math.floor(Math.random() * 100) + 1, "totalPerformance": Math.floor(Math.random() * 100) + 1 },
    { "date": "2024-01-09", "avgPerformance": Math.floor(Math.random() * 100) + 1, "totalPerformance": Math.floor(Math.random() * 100) + 1 },
    { "date": "2024-01-13", "avgPerformance": Math.floor(Math.random() * 100) + 1, "totalPerformance": Math.floor(Math.random() * 100) + 1 },
    { "date": "2024-01-17", "avgPerformance": Math.floor(Math.random() * 100) + 1, "totalPerformance": Math.floor(Math.random() * 100) + 1 },
    { "date": "2024-01-21", "avgPerformance": Math.floor(Math.random() * 100) + 1, "totalPerformance": Math.floor(Math.random() * 100) + 1 },
    { "date": "2024-01-25", "avgPerformance": Math.floor(Math.random() * 100) + 1, "totalPerformance": Math.floor(Math.random() * 100) + 1 },
    { "date": "2024-01-29", "avgPerformance": Math.floor(Math.random() * 100) + 1, "totalPerformance": Math.floor(Math.random() * 100) + 1 },
    { "date": "2024-02-02", "avgPerformance": Math.floor(Math.random() * 100) + 1, "totalPerformance": Math.floor(Math.random() * 100) + 1 },
    { "date": "2024-02-06", "avgPerformance": Math.floor(Math.random() * 100) + 1, "totalPerformance": Math.floor(Math.random() * 100) + 1 },
];


  const geoSampleData =  [
    {
      "date": "2024-01-01",
      "metrics": [
        { "location": "United States", "counts": 1500000 },
        { "location": "China", "counts": 1800000 },
        { "location": "India", "counts": 1600000 },
        { "location": "Brazil", "counts": 1400000 },
        { "location": "Pakistan", "counts": 1350000 },
        { "location": "Nigeria", "counts": 1450000 }
      ]
    },
    {
      "date": "2024-01-02",
      "metrics": [
        { "location": "Bangladesh", "counts": 1700000 },
        { "location": "Russia", "counts": 1550000 },
        { "location": "Mexico", "counts": 1650000 },
        { "location": "Japan", "counts": 1300000 },
        { "location": "Ethiopia", "counts": 1250000 },
        { "location": "Philippines", "counts": 1450000 }
      ]
    },
    {
      "date": "2024-01-03",
      "metrics": [
        { "location": "Egypt", "counts": 1400000 },
        { "location": "Vietnam", "counts": 1200000 },
        { "location": "DR Congo", "counts": 1150000 },
        { "location": "Turkey", "counts": 1100000 },
        { "location": "Iran", "counts": 1000000 },
        { "location": "Germany", "counts": 950000 }
      ]
    },
    {
      "date": "2024-01-04",
      "metrics": [
        { "location": "Thailand", "counts": 1050000 },
        { "location": "United Kingdom", "counts": 900000 },
        { "location": "France", "counts": 850000 }
      ]
    }
  ]

  console.log("Hi, I got request");
  try {
    const result = await Application.findOneAndUpdate(
      { applicationId: "57HQZ2J" },
      { $push: { "services.PERFORMANCEMETRIC": { $each: sampleData } } },
      { new: true }
    ).exec();

    console.log("Documents added successfully:", result);
    res.status(200).json({ message: "Dummy data inserted successfully" });
  } catch (error) {
    console.error("Error occurred:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

