const mongoose = require("mongoose");

//import schemas
const Tenant = require("../models/TenantSchema");
const generateId = require("../helpers/generateId");
const { createToken, verifyToken } = require("../middleware/middleware");
const jwt = require("jsonwebtoken");

exports.temp = (req, res) => {
  res.json({ message: "In auth controller" });
};
exports.verifyUser = async (req, res) => {
  const token = req.cookies.jwt;
  console.log("In verifyUser, cookie is: " + token);

  // if (!token) {
  //   return res.status(400).json({ message: "Token is required" });
  // }

  verifyToken(req, res, () => {
    const decoded = req.decoded;
    console.log(decoded);
    console.log("Token was validated!");
    res.status(200).json({
      message: "Token is valid",
      tenantId: decoded.tenantId,
      tenantName: decoded.tenantName,
    });
  });
};

exports.login = async (req, res) => {
  const { email, password } = req.body;

  try {
    const response = await Tenant.findOne(
      { tenantMail: email, tenantAuth: password },
      "tenantId tenantName tenantMail tenantMembers"
    );

    if (!response) {
      return res.status(400).json({
        message: "User does not exist.",
      });
    }

    req.user = { tenantId: response.tenantId, tenantName: response.tenantName };

    createToken(req, res);
    const token = res.locals.token;
    res.cookie("jwt", token, { maxAge: 3600000 }); // 1 hour expiration
    res.cookie("tenantName", response.tenantName, {
      maxAge: 360000,
    });
    res.cookie("tenantId", response.tenantId, {
      maxAge: 360000,
    });
    console.log(res);
    res.user = { tenantId: response.tenantId, tenantName: response.tenantName };
    console.log("Cookie was created");
    res.status(200).json(response);
  } catch (error) {
    console.error("Error: ", error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

exports.register = async (req, res) => {
  const { name, email, password } = req.body;
  const id = generateId();

  try {
    const newTenant = await Tenant.create({
      tenantId: id,
      tenantName: name,
      tenantMail: email,
      tenantAuth: password,
    });

    console.log("New tenant created:", newTenant);
    res.status(201).json({
      message: "Tenant created successfully",
      data: newTenant,
    });
  } catch (error) {
    if (error.code === 11000) {
      console.error(
        "Error: Duplicate key violation. The email address must be unique."
      );
      res.status(409).json({
        message: "User already exists.",
      });
    } else {
      console.error("Error creating tenant:", error);
      res.status(500).json({
        message: "Internal Server Error",
      });
    }
  }
};
