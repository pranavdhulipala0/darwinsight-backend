const mongoose = require("mongoose");
const Application = require("../models/ApplicationSchema");
const Services = require("../models/ServicesSchema");
const generateId = require("../helpers/generateId");
const Tenant = require("../models/TenantSchema");

exports.test = async (req, res) => {
  try {
    res.status(200).send("Welcome to API");
  } catch (error) {
    res.status(500).send({ message: "Internal Server Error" });
  }
};

exports.createIntegration = async (req, res) => {
  const { tenantId, applicationName, applicationUrl } = req.body;
  const applicationId = generateId();

  try {
    const tenantResponse = await Tenant.findOne({ tenantId });
    if (!tenantResponse) {
      return res.status(400).send({ message: "Invalid Tenant ID!" });
    }

    const response = await Application.create({
      tenantId,
      applicationId,
      applicationName,
      applicationUrl,
    });

    if (response) {
      res.status(200).send({
        message: "Integration created",
        applicationId: response.applicationId,
      });
    } else {
      res.status(500).send({
        message: "Internal Server Error",
      });
    }
  } catch (error) {
    res.status(500).send({ message: "Error: " + error.message });
  }
};

exports.fetchIntegrations = async (req, res) => {
  const tenantId = req.body.tenantId;

  try {
    const data = await Application.find(
      { tenantId: tenantId },
      "applicationId applicationName applicationUrl services"
    );

    if (!data) {
      res.status(404).send({ message: "No data found." });
    } else {
      res.status(200).send(data);
    }
  } catch (error) {
    res.status(500).send({ message: "Error fetching integrations" });
  }
};

exports.fetchApplicationData = async (req, res) => {
  const { tenantId, applicationId } = req.body;

  try {
    const data = await Application.findOne({
      tenantId: tenantId,
      applicationId: applicationId,
    });

    if (!data) {
      res.status(404).send({ message: "No data found." });
    } else {
      res.status(200).send(data);
    }
  } catch (error) {
    res.status(500).send({ message: "Error fetching application data!" });
  }
};
