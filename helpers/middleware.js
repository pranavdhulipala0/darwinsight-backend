const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");

dotenv.config();

module.exports.verifyToken = (req, res, next) => {
  console.log(req.cookies);
  if (!req.cookies) return next();
  const token = req.cookies.jwt;
  if (!token) return next();
  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.user = decoded;
    return next();
  } catch (error) {
    return res.status(401).json({ message: "Invalid token." });
  }
};

module.exports.checkToken = (token) => {
  if(!token){
    return false;
  }
  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    if(decoded){
      return true;
    }
    return false;
  } catch (error) {
    return false;
  }
};

module.exports.createToken = (tenantId, tenantName) => {
  return jwt.sign(
    { tenantId: tenantId, tenantName: tenantName },
    process.env.JWT_SECRET,
    { expiresIn: "1h" }
  );
};
