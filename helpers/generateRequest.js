navigator.geolocation.getCurrentPosition(position => {
    const { latitude, longitude } = position.coords;
    callDarwinsight(latitude, longitude);
});

function callDarwinsight(latitude, longitude) {

    fetch('localhost:5000/api/service/geometric', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "tenantId":"Your Tenant ID here",
            "applicationId" : "Your application ID here",
            "latitude" : latitude,
            "longitude" : longitude
        })
    });
}
