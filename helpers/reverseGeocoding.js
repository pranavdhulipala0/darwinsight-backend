
async function reverseGeocode(latitude,longitude){
    const url = `https://nominatim.openstreetmap.org/reverse?format=json&lat=${latitude}&lon=${longitude}`;
    try{
        const response = await fetch(url);
        const data = await response.json();
        return data.address.country;

    }
    catch{
        return null;
    }
    // console.log(data.address.country);
}

module.exports = reverseGeocode;