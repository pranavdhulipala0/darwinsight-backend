const jwt = require("jsonwebtoken");

function createToken(req, res, next) {
  const user = req.user;
  const secretKey = process.env.JWT_SECRET_KEY;
  const token = jwt.sign(
    { tenantId: user.tenantId, tenantName: user.tenantName },
    secretKey,
    { expiresIn: "1h" }
  );
  res.locals.token = token;
}

function verifyToken(req, res, next) {
  const token = req.cookies.jwt;

  if (!token) {
    return res.status(401).json({ message: "No token provided" });
  }

  const secretKey = process.env.JWT_SECRET_KEY;

  jwt.verify(token, secretKey, (err, decoded) => {
    if (err) {
      return res.status(403).json({ message: "Failed to authenticate token" });
    } else {
      req.decoded = decoded;
      next();
    }
  });
}

module.exports = {
  createToken,
  verifyToken,
};
