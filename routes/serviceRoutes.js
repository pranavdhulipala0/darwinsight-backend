const express = require('express');
const   router = express.Router();
const serviceController = require("../controllers/serviceController.js");

router.get("/test",serviceController.test)
router.post("/countmetric",serviceController.countMetric)
router.post("/geometric",serviceController.geoMetric)
router.post("/searchmetric",serviceController.searchMetric)
router.post("/performancemetric",serviceController.performanceMetric)

router.post("/fetchMetrics",serviceController.fetchMetrics);
router.get("/insertDummyData",serviceController.insertDummyData)


// router.post("/search",serviceController.search)

module.exports = router;