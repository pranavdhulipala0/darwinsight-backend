const express = require("express");
const router = express.Router();

const serviceRoutes = require("./serviceRoutes.js");
const appRoutes = require("./appRoutes.js");
const authRoutes = require("./authRoutes.js");
const { verifyToken } = require("../middleware/middleware.js");

router.use("/service", serviceRoutes);
router.use("/auth", authRoutes);
router.use("/application", verifyToken, appRoutes);

module.exports = router;
