const express = require('express');
const router = express.Router();
const applicationController = require("../controllers/applicationController.js");

router.post("/createIntegration",applicationController.createIntegration)
router.post("/fetchIntegrations",applicationController.fetchIntegrations)
router.post("/fetchApplicationData",applicationController.fetchApplicationData)


module.exports = router;