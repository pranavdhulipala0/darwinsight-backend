const chai = require('chai');
const sinon = require('sinon');
const AuthController = require('../controllers/authController');
const Tenant = require('../models/TenantSchema');
const { createToken, verifyToken } = require('../middleware/middleware');
const expect = chai.expect;

describe('AuthController', () => {
  describe('temp', () => {
    it('should return a JSON object with message "In auth controller"', () => {
      const req = {};
      const res = {
        json: sinon.spy()
      };

      AuthController.temp(req, res);

      expect(res.json.calledOnce).to.be.true;
      expect(res.json.firstCall.args[0]).to.deep.equal({ message: 'In auth controller' });
    });

  });

  describe('verifyUser', () => {
    it('should return status code 400 if token is missing', () => {
      const req = {
        cookies: {}
      };
      const res = {
        status: sinon.stub().returnsThis(),
        json: sinon.spy()
      };

      AuthController.verifyUser(req, res);

      expect(res.status.calledOnceWith(400)).to.be.true;
      expect(res.json.calledOnceWith({ message: 'Token is required' })).to.be.true;
    });

  });

  describe('login', () => {
    it('should return status code 400 if user does not exist', async () => {
      const req = {
        body: { email: 'nonexistent@example.com', password: 'password123' }
      };
      const res = {
        status: sinon.stub().returnsThis(),
        json: sinon.spy(),
        cookie: sinon.spy()
      };

      sinon.stub(Tenant, 'findOne').resolves(null);

      await AuthController.login(req, res);

      expect(res.status.calledOnceWith(400)).to.be.true;
      expect(res.json.calledOnceWith({ message: 'User does not exist.' })).to.be.true;

      Tenant.findOne.restore();
    });

  });

  describe('register', () => {
    it('should return status code 201 and create a new tenant', async () => {
      const req = {
        body: { name: 'John Doe', email: 'john@example.com', password: 'password123' }
      };
      const res = {
        status: sinon.stub().returnsThis(),
        json: sinon.spy()
      };

      sinon.stub(Tenant, 'create').resolves({
        tenantId: '123',
        tenantName: 'John Doe',
        tenantMail: 'john@example.com',
        tenantAuth: 'password123'
      });

      await AuthController.register(req, res);

      expect(res.status.calledOnceWith(201)).to.be.true;
      expect(res.json.calledOnce).to.be.true;
      expect(res.json.firstCall.args[0]).to.deep.equal({
        message: 'Tenant created successfully',
        data: {
          tenantId: '123',
          tenantName: 'John Doe',
          tenantMail: 'john@example.com',
          tenantAuth: 'password123'
        }
      });

      Tenant.create.restore();
    });

  });
});
