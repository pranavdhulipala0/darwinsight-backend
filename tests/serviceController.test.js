const chai = require('chai');
const sinon = require('sinon');
const ServiceController = require('../controllers/serviceController');
const Application = require('../models/ApplicationSchema');
const reverseGeocode = require('../helpers/reverseGeocoding');
const expect = chai.expect;

describe('ServiceController', () => {
  describe('temp', () => {
    it('should return a JSON object with message "In auth controller"', () => {
      const req = {};
      const res = {
        json: sinon.spy()
      };

      ServiceController.temp(req, res);

      expect(res.json.calledOnce).to.be.true;
      expect(res.json.firstCall.args[0]).to.deep.equal({ message: 'In auth controller' });
    });

    it('should return status code 200', () => {
      const req = {};
      const res = {
        json: sinon.spy()
      };

      ServiceController.temp(req, res);

      expect(res.json.calledOnce).to.be.true;
      expect(res.json.firstCall.args[0]).to.deep.equal({ message: 'In auth controller' });
    });
  });

  describe('test', () => {
    it('should return "Welcome to API"', () => {
      const req = {};
      const res = {
        send: sinon.spy()
      };

      ServiceController.test(req, res);

      expect(res.send.calledOnce).to.be.true;
      expect(res.send.firstCall.args[0]).to.equal('Welcome to API');
    });

    it('should return status code 200', () => {
      const req = {};
      const res = {
        send: sinon.spy()
      };

      ServiceController.test(req, res);

      expect(res.send.calledOnce).to.be.true;
      expect(res.send.firstCall.args[0]).to.equal('Welcome to API');
    });
  });

  describe('searchMetric', () => {
    it('should return status code 404 if application is not found', async () => {
      const req = {
        body: {
          tenantId: 'tenant123',
          applicationId: 'app123',
          query: 'test query'
        }
      };
      const res = {
        status: sinon.stub().returnsThis(),
        json: sinon.spy()
      };

      sinon.stub(Application, 'findOne').resolves(null);

      await ServiceController.searchMetric(req, res);

      expect(res.status.calledOnceWith(404)).to.be.true;
      expect(res.json.calledOnceWith({ error: 'Application not found' })).to.be.true;

      Application.findOne.restore();
    });

    it('should return status code 200 if metric is updated successfully', async () => {
      const req = {
        body: {
          tenantId: 'tenant123',
          applicationId: 'app123',
          query: 'test query'
        }
      };
      const res = {
        status: sinon.stub().returnsThis(),
        json: sinon.spy()
      };

      sinon.stub(Application, 'findOne').resolves({
        services: { SEARCHMETRIC: [] },
        save: sinon.stub().resolves()
      });

      await ServiceController.searchMetric(req, res);

      expect(res.status.calledOnceWith(200)).to.be.true;
      expect(res.json.calledOnceWith({ message: 'Metric updated successfully' })).to.be.true;

      Application.findOne.restore();
    });
  });

  describe('geoMetric', () => {
    it('should return status code 404 if application is not found', async () => {
      const req = {
        body: {
          tenantId: 'tenant123',
          applicationId: 'app123',
          latitude: 123,
          longitude: 456
        }
      };
      const res = {
        status: sinon.stub().returnsThis(),
        json: sinon.spy()
      };

      sinon.stub(Application, 'findOne').resolves(null);

      await ServiceController.geoMetric(req, res);

      expect(res.status.calledOnceWith(404)).to.be.true;
      expect(res.json.calledOnceWith({ error: 'Application not found' })).to.be.true;

      Application.findOne.restore();
    });

    // Add another test case for geoMetric if needed
  });

  describe('countMetric', () => {
    it('should return status code 404 if application is not found', async () => {
      const req = {
        body: {
          tenantId: 'tenant123',
          applicationId: 'app123'
        }
      };
      const res = {
        status: sinon.stub().returnsThis(),
        json: sinon.spy()
      };

      sinon.stub(Application, 'findOne').resolves(null);

      await ServiceController.countMetric(req, res);

      expect(res.status.calledOnceWith(404)).to.be.true;
      expect(res.json.calledOnceWith({ error: 'Application not found' })).to.be.true;

      Application.findOne.restore();
    });

    it('should increment count and return status code 200', async () => {
      const req = {
        body: {
          tenantId: 'tenant123',
          applicationId: 'app123'
        }
      };
      const res = {
        status: sinon.stub().returnsThis(),
        json: sinon.spy()
      };

      const findOneStub = sinon.stub(Application, 'findOne').resolves({
        services: { COUNTMETRIC: [{ date: new Date(), counts: 1 }] },
        save: sinon.stub().resolves()
      });

      await ServiceController.countMetric(req, res);

      expect(findOneStub.calledOnce).to.be.true;
      expect(res.status.calledOnceWith(200)).to.be.true;
      expect(res.json.calledOnceWith({ message: 'Count incremented successfully' })).to.be.true;

      Application.findOne.restore();
    });
  });

  describe('performanceMetric', () => {
    it('should return status code 404 if application is not found', async () => {
      const req = {
        body: {
          tenantId: 'tenant123',
          applicationId: 'app123',
          performance: 10
        }
      };
      const res = {
        status: sinon.stub().returnsThis(),
        json: sinon.spy()
      };

      sinon.stub(Application, 'findOne').resolves(null);

      await ServiceController.performanceMetric(req, res);

      expect(res.status.calledOnceWith(404)).to.be.true;
      expect(res.json.calledOnceWith({ error: 'Application not found' })).to.be.true;

      Application.findOne.restore();
    });

    it('should record performance and return status code 200', async () => {
      const req = {
        body: {
          tenantId: 'tenant123',
          applicationId: 'app123',
          performance: 10
        }
      };
      const res = {
        status: sinon.stub().returnsThis(),
        json: sinon.spy()
      };

      const findOneStub = sinon.stub(Application, 'findOne').resolves({
        services: { PERFORMANCEMETRIC: [{ date: new Date(), count: 1, totalPerformance: 10 }] },
        save: sinon.stub().resolves()
      });

      await ServiceController.performanceMetric(req, res);

      expect(findOneStub.calledOnce).to.be.true;
      expect(res.status.calledOnceWith(200)).to.be.true;
      expect(res.json.calledOnceWith({ message: 'Performance recorded successfully' })).to.be.true;

      Application.findOne.restore();
    });
  });

  describe('fetchMetrics', () => {
    it('should return metrics', async () => {
      const req = {
        body: {
          tenantId: 'tenant123',
          applicationId: 'app123',
          metricType: 'SEARCHMETRIC',
          startDate: '2024-01-01',
          endDate: '2024-01-31'
        }
      };
      const res = {
        json: sinon.spy()
      };

      const aggregateStub = sinon.stub(Application, 'aggregate').resolves([]);

      await ServiceController.fetchMetrics(req, res);

      expect(aggregateStub.calledOnce).to.be.true;
      expect(res.json.calledOnce).to.be.true;

      aggregateStub.restore();
    });

    
  });
});

