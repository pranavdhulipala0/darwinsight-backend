const chai = require('chai');
const sinon = require('sinon');
const Application = require('../models/ApplicationSchema');
const Tenant = require('../models/TenantSchema');
const generateId = require('../helpers/generateId');
const IntegrationController = require('../controllers/applicationController');
const expect = chai.expect;

describe('IntegrationController', () => {
  describe('test', () => {
    it('should return status code 200 with message "Welcome to API"', () => {
      const req = {};
      const res = {
        status: sinon.stub().returnsThis(),
        send: sinon.spy()
      };

      IntegrationController.test(req, res);

      expect(res.status.calledOnceWith(200)).to.be.true;
      expect(res.send.calledOnceWith('Welcome to API')).to.be.true;
    });
  });

  describe('createIntegration', () => {
    it('should return status code 400 if tenant ID is invalid', async () => {
      const req = {
        body: {
          tenantId: 'invalidId',
          applicationName: 'Test Application',
          applicationUrl: 'http://example.com'
        }
      };
      const res = {
        status: sinon.stub().returnsThis(),
        send: sinon.spy()
      };

      sinon.stub(Tenant, 'findOne').resolves(null);

      await IntegrationController.createIntegration(req, res);

      expect(res.status.calledOnceWith(400)).to.be.true;
      expect(res.send.calledOnceWith({ message: 'Invalid Tenant ID!' })).to.be.true;

      Tenant.findOne.restore();
    });

    // Add more test cases for createIntegration if needed
  });

  describe('fetchIntegrations', () => {
    it('should return status code 404 if no data found', async () => {
      const req = {
        body: {
          tenantId: 'validTenantId'
        }
      };
      const res = {
        status: sinon.stub().returnsThis(),
        send: sinon.spy()
      };

      sinon.stub(Application, 'find').resolves(null);

      await IntegrationController.fetchIntegrations(req, res);

      expect(res.status.calledOnceWith(404)).to.be.true;
      expect(res.send.calledOnceWith({ message: 'No data found.' })).to.be.true;

      Application.find.restore();
    });

    // Add more test cases for fetchIntegrations if needed
  });

  describe('fetchApplicationData', () => {
    it('should return status code 404 if no data found', async () => {
      const req = {
        body: {
          tenantId: 'validTenantId',
          applicationId: 'validApplicationId'
        }
      };
      const res = {
        status: sinon.stub().returnsThis(),
        send: sinon.spy()
      };

      sinon.stub(Application, 'findOne').resolves(null);

      await IntegrationController.fetchApplicationData(req, res);

      expect(res.status.calledOnceWith(404)).to.be.true;
      expect(res.send.calledOnceWith({ message: 'No data found.' })).to.be.true;

      Application.findOne.restore();
    });

    // Add more test cases for fetchApplicationData if needed
  });
});
