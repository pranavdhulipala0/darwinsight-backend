const mongoose = require("mongoose");

const Services = {
  COUNTER: new mongoose.Schema({
    statistics: [
      {
        date: {
          type: Date,
          required: true,
        },
        counts: {
          type: Number,
          default: 0,
        },
      },
    ],
    default:{statistics:[]}
  }),
  SEARCH: new mongoose.Schema({
    statistics: [
      {
        date: {
          type: Date,
          required: true,
        },
        searchQueries: {
          type: [String],
          default: [],
        },
      },
    ],
  }),
};

module.exports = Services;
