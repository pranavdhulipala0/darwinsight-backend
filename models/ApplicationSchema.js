const mongoose = require("mongoose");
const Services = require("./ServicesSchema.js");
const ServiceSchema = new mongoose.Schema({
  "COUNTMETRIC": [
      {
        date: {
          type: Date,
          required: true,
        },
        counts: {
          type: Number,
          default: 0,
        },
      },
    ],
  "SEARCHMETRIC":[
    {
      date: {
        type: Date,
        required: true,
      },
      searchQueries: {
        type: [String],
      },
    },
  ],
  "GEOMETRIC": [
    {
      date: {
        type: Date,
        required: true,
      },
      metrics: [
        {
          location: {
            type: String,
            required: true,
          },
          counts: {
            type: Number,
            default: 0,
          },
        },
      ],
    },
  ],
  "PERFORMANCEMETRIC": [
    {
      date: {
        type: Date,
        required: true,
      },
      avgPerformance: {
        type: Number,
        default: 0,
      },
      totalPerformance: {
        type: Number,
        default: 0,
      },
      count:{
        type:Number,
        default: 1
      }
    },
  ],
  
},{
  _id: false,
  strict: false,
  minimize: false
  })
const ApplicationSchema = new mongoose.Schema({
  tenantId: {
    type: String,
    required: true,
  },
  applicationId: {
    type: String,
    required: true,
  },
  applicationName: {
    type: String,
    required: true,
  },
  applicationUrl: {
    type: String,
    default: "None provided",
  },
  services: {
    type: ServiceSchema,
    default:{}
  },
},{
  strict: false,
  minimize: false
  });

const Application = mongoose.model("Application", ApplicationSchema);
module.exports = Application;
