const mongoose = require('mongoose');

const tenantSchema = new mongoose.Schema({
  tenantId: {
    type: String,
    unique:true,
    required: true
  },
  tenantName: {
    type: String,
    required: true
  },
  tenantMail:{
    type:String,
    unique:true,
    required: true
  },
  tenantAuth: {
    type: String,
    required: true
  },
  tenantMembers: [{
    empMail: {
      type: String,
    }
    ,default:[]
  }],
  applications:[String]
});

const Tenant = mongoose.model('Tenant', tenantSchema);

module.exports = Tenant;
